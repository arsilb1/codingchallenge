var pack = class pack {
  constructor(packQuantity, packRate) {
    this.packQuantity = packQuantity;
    this.packRate = packRate;
  }
};

// Start function from where application can start
function start() {
  
  const at5Pack1 = new pack(3, 6.99);
  const at5Pack2 = new pack(5, 8.99);

  var at5PackArray =  [at5Pack1, at5Pack2];
  var at5PackQuantityArray = [at5Pack1.packQuantity, at5Pack2.packQuantity];

  const mb11Pack1 = new pack(2, 9.95);
  const mb11Pack2 = new pack(5, 16.95);
  const mb11Pack3 = new pack(8, 24.95);

  var mb11PackArray =  [mb11Pack1, mb11Pack2, mb11Pack3];
  var mb11PackQuantityArray = [mb11Pack1.packQuantity, mb11Pack2.packQuantity, mb11Pack3.packQuantity];

  const cfPack1 = new pack(3, 5.95);
  const cfPack2 = new pack(5, 9.95);
  const cfPack3 = new pack(9, 16.99);

  var cfPackArray =  [cfPack1, cfPack2, cfPack3];
  var cfPackQuantityArray = [cfPack1.packQuantity, cfPack2.packQuantity, cfPack3.packQuantity];

  var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('./inputText.txt')
  });

  lineReader.on('line', function (textLine) {
        var item;
        var words = textLine.split(" ");
        item = words[1];
        switch(item) {
          case 'AT5':
          case 'At5':
              displayOutput(words[0], at5PackArray, at5PackQuantityArray, textLine, true);
              break;
          case 'MB11':
          case 'mb11':
              displayOutput(words[0], mb11PackArray, mb11PackQuantityArray, textLine, true);
              break;
          case 'CF':
          case 'cf':
              displayOutput(words[0], cfPackArray, cfPackQuantityArray, textLine, true);
              break;
          default:
              displayOutput(0, [], [], line, false);
              break;
        }
  });
  return "";
}

function displayOutput(packReq, packArray, packQuantityArray, inputItem, isItemPresent) {
  if (isItemPresent) {
    var firstWords = [];
    var words = inputItem.split(" ");
    firstWords.push(words[0]);
    var packReq = Number(firstWords);
    if (isNaN(packReq) == false) {
        var output = rephraseInput(packReq, packQuantityArray);
        if (output.succ != 0) {
            var tot_price = calculatePrice(output.resArr, output.pack, packArray);
            tot_price = Math.round(tot_price * 100) / 100
        } else {
            tot_price = 0;
            output.resArr = [];
        }
        console.log(words[0] + " " + words[1] + " $" + tot_price);
        for (var j = 0; j < output.resArr.length; j++) {
          for (var k = 0; k < packArray.length; k++) {
            if (packArray[k].packQuantity == output.pack[j]) {
              console.log(output.resArr[j].quot +
                    " x " + output.pack[j] + " $" + packArray[k].packRate);
            }
          }
        }
        console.log("----")
    } else {
      console.log("Item count is not a packReq.")
    }
  } else {
    if (inputItem == "") {
    } else {
      console.log("Item not present.")
    }
  }
}

function rephraseInput(packReq, array) {
    array.sort();
    array.reverse();
    var ret;
    var temp_arr = array.slice(0);
    var flag = 0;
    while (temp_arr.length > 0) {
        ret = subData(packReq, temp_arr);
        if (ret.ret != -1) {
          flag = 1;
          break;
        } else {
          temp_arr.splice(0, 1);
        }
    }

    return {
      succ: flag,
      resArr: ret.resArr,
      pack: ret.val
    }
}


function subData(packReq, array) {
    var resArr = [];
    var ptr, end, first = [];
    var ret;
    end = array.length - 1;
    ptr = 0;
    var flag = 0;
    var temp_arr = array.slice(0);
    first.push(array[0]);
      while ((ptr <= end)) {
          ptr++;
          var splicedArr = temp_arr.slice(ptr,end + 1);
          var newArr = first.concat(splicedArr);
          resArr = arrayModul(packReq, newArr, []);
          ret = checkZero(resArr);
          if (ret >= 0) {
            break;
          } else {
          }
      }
      return {
        ret: ret,
        resArr: resArr,
        val: newArr
      }
}


function checkZero(array) {
    var flag = 0;

    for(var i = 0; i < array.length; ++i) {
      if(array[i].rem == 0) {
        flag = 1;
        break;
      }
    }
    if(flag) {
      return i;
    } else {
      return -1;
    }
}

let divideRemainder = function(dividend, divisor) {
  var quot = Math.floor(dividend / divisor);
  var rem = dividend % divisor;

  return {quot: quot,
          rem: rem}
}

function arrayModul(packReq, array, resArr) {
    var tempArr = array.slice(0);
    var quot = -1;
    var rem = -1;
    var max = Math.max(...tempArr);
    var index = tempArr.indexOf(max);
    var ans = divideRemainder(packReq, max);
    quot = ans.quot;
    rem = ans.rem;
    if (index > -1) {
      tempArr.splice(index, 1);
    }
    resArr.push({rem: rem, quot: quot});
    return ((tempArr.length > 0) && (rem != 0)) ?
        arrayModul(rem, tempArr, resArr) : resArr;
}

function calculatePrice (resArr, resPack, packs) {
  var tot_price = 0;
  for (var j = 0; j < resArr.length; j++) {
      for (var k = 0; k < packs.length; k++) {
        if (packs[k].packQuantity == resPack[j]) {
          var count = resArr[j].quot;
          var price = count * packs[k].packRate;
          tot_price += price;
        }
      }
  }
  return tot_price;
}

module.exports = {
  start,
  divideRemainder,
  rephraseInput,
  calculatePrice,
  subData,
  arrayModul,
  checkZero,
  displayOutput
}

require('make-runnable/custom') ({
    printOutputFrame: false
})
